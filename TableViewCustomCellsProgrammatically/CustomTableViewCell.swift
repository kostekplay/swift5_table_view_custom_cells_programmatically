////  CustomTableViewCell.swift
//  TableViewCustomCellsProgrammatically
//
//  Created on 02/09/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let indentyfier = "CustomTableViewCell"
    
    private let mySwitch: UISwitch = {
        let sw = UISwitch()
        sw.isOn = true
        sw.onTintColor = .blue
        return sw
    }()
    
    private let myLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = .white
        lb.font = .systemFont(ofSize: 17, weight: .bold)
        lb.text = "Cell Custom"
        return lb
    }()
    
    private let myImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(systemName: "star")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .orange
        
        contentView.addSubview(mySwitch)
        contentView.addSubview(myLabel)
        contentView.addSubview(myImageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let imageSize = contentView.frame.size.height - 6
        
        mySwitch.frame = CGRect(x: 5,
                                y: 5,
                                width: 100,
                                height: contentView.frame.size.height - 10)
        
        myLabel.frame = CGRect(x: 10 + mySwitch.frame.size.width,
                               y: 5,
                               width: contentView.frame.size.width - 10 - mySwitch.frame.size.width - imageSize,
                               height: contentView.frame.size.height - 10)
        
        myImageView.frame = CGRect(x: contentView.frame.size.width - imageSize, y: 3, width: imageSize, height: imageSize)
    }
    
}
